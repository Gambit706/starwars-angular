export class Planets {
    constructor(public name: string,
        public rotation_period: number,
        public orbital_period: number,
        public diameter: number,
        public climate: string,
        public gravity: string,
        public terrain: string,
        public surface_water: string,
        public residents: number,
        public films: string,
        public created: string,
        public edited: string,
        public url: string) {}
}
