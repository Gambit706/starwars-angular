export class People {

    constructor(public name: string,
                public height: number,
                public mass: number,
                public hair_color: string,
                public skin_color: string,
                public eye_color: string,
                public birth_year: string,
                public gender: string,
                public films: string,
                public species: string,
                public vehicles: string,
                public starships: string,
                public created: string,
                public edited: string,
                public url: string) {}
}
