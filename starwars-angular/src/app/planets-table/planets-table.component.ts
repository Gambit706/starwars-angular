import { Component, OnInit } from '@angular/core';
import { Planets } from '../model/planets';
import { PlanetsService } from '../services/planets.service';

@Component({
  selector: 'app-planets-table',
  templateUrl: './planets-table.component.html',
  styleUrls: ['./planets-table.component.css']
})
export class PlanetsTableComponent implements OnInit {

  planets: Planets[];

  constructor(private planetsService: PlanetsService) { }

  ngOnInit() {
  }

}
