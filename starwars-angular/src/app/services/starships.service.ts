import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Starships } from '../model/starships';

@Injectable({
  providedIn: 'root'
})
export class StarshipsService {

  constructor(private httpClient: HttpClient) { }

  public getStarships(): Observable<Starships[]> {
    return this.httpClient.get<Starships[]>(
              environment.backendUrl + '/starships');
  }
}
