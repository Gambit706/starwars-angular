import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Films } from '../model/films';

@Injectable({
  providedIn: 'root'
})
export class FilmsService {
  newFilmObservable: any;

  constructor(private httpClient: HttpClient) { }

  public getFilms(): Observable<any> {
    return this.httpClient.get<any>(
              environment.backendUrl + '/films');
  }

  public findFilms(enteredValue) {
    return this.httpClient.get<any>(
              environment.backendUrl + '/films');
  }
}
