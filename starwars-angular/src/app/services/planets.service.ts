import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Planets } from '../model/planets';

@Injectable({
  providedIn: 'root'
})
export class PlanetsService {

  constructor(private httpClient: HttpClient) { }

  public getPlanets(): Observable<Planets[]> {
    return this.httpClient.get<Planets[]>(
              environment.backendUrl + '/planets');
  }
}
