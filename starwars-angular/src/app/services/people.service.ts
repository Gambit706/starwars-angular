import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { People } from '../model/people';

@Injectable({
  providedIn: 'root'
})
export class PeopleService {

  constructor(private httpClient: HttpClient) { }

  public getPeople(): Observable<any> {
    return this.httpClient.get<any>(
              environment.backendUrl + '/films');
  }
}
