import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Species } from '../model/species';

@Injectable({
  providedIn: 'root'
})
export class SpeciesService {

  constructor(private httpClient: HttpClient) { }

  public getSpecies(): Observable<Species[]> {
    return this.httpClient.get<Species[]>(
              environment.backendUrl + '/species');
  }
}
