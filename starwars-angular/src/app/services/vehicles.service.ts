import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Vehicles } from '../model/vehicles';

@Injectable({
  providedIn: 'root'
})
export class VehiclesService {

  constructor(private httpClient: HttpClient) { }

  public getVehicles(): Observable<Vehicles[]> {
    return this.httpClient.get<Vehicles[]>(
              environment.backendUrl + '/vehicles');
  }
}
