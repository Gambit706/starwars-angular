import { Component, OnInit } from '@angular/core';
import { Starships } from '../model/starships';
import { StarshipsService } from '../services/starships.service';

@Component({
  selector: 'app-starships-table',
  templateUrl: './starships-table.component.html',
  styleUrls: ['./starships-table.component.css']
})
export class StarshipsTableComponent implements OnInit {

  starship: Starships[];

  constructor(private starshipsService: StarshipsService) { }

  ngOnInit() {
  }

}
