import { Component, OnInit } from '@angular/core';
import { SpeciesService } from '../services/species.service';
import { Species } from '../model/species';

@Component({
  selector: 'app-species-table',
  templateUrl: './species-table.component.html',
  styleUrls: ['./species-table.component.css']
})
export class SpeciesTableComponent implements OnInit {

  species: Species[];

  constructor(private speciesService: SpeciesService) { }

  ngOnInit() {
  }

}
