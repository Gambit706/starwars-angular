import { Component, OnInit } from '@angular/core';
import { FilmsService } from '../services/films.service';
import { Films } from '../model/films';

@Component({
  selector: 'app-films-table',
  templateUrl: './films-table.component.html',
  styleUrls: ['./films-table.component.css']
})
export class FilmsTableComponent implements OnInit {


  films: Films[];
  findFilms: findFilms[];
  constructor(private filmsService: FilmsService) {}

ngOnInit() {
    this.filmsService.getFilms().subscribe(
      resultsSet => {
        console.log(resultsSet);
        this.films = resultsSet.results;
        console.log(this.films);
      },
      error => {
        console.log(error);
        alert('Failed to retrieve films: ' + error.message);
      }
    );
  }
}

