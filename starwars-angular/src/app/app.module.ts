import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { PeopleTableComponent } from './people-table/people-table.component';
import { PlanetsTableComponent } from './planets-table/planets-table.component';
import { FilmsTableComponent } from './films-table/films-table.component';
import { SpeciesTableComponent } from './species-table/species-table.component';
import { VehiclesTableComponent } from './vehicles-table/vehicles-table.component';
import { StarshipsTableComponent } from './starships-table/starships-table.component';
import { SearchBarComponent } from './search-bar/search-bar.component';

@NgModule({
  declarations: [
    AppComponent,
    PeopleTableComponent,
    PlanetsTableComponent,
    FilmsTableComponent,
    SpeciesTableComponent,
    VehiclesTableComponent,
    StarshipsTableComponent,
    SearchBarComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
